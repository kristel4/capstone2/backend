const express = require('express')
const router = express.Router()
const auth = require('../auth')
const UserController = require('../controllers/user')

// [SECTION] Primary Routes
//check if email exists
router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result))
})

//registration of a user
router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})

//logging in of a user
router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})

//getting the information of a user and displaying it to his/her profile
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(user => res.send(user))
})

//user enrollment to a course
router.post('/enroll', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId,
        fullName: req.body.fullName
	}
    UserController.enroll(params).then(result => res.send(result))
})

//updating course if admin
router.put('/', auth.verify, (req, res) => {
    CourseController.update(req.body).then(result => res.send(result))
})

// [SECTION] Secondary Routes

//update details of a user
router.put('/updating-details', auth.verify, (req, res) => {
    UserController.update(req.body).then(result => res.send(result))
})



//update avatar
router.put('/details', auth.verify, (req, res) => {
    UserController.update(req.body).then(result => res.send(result))
})

//HINDI PA TAPOS!
//change password of a user
router.put('/change-password', (req, res) => {
    UserController.changePassword(req.body).then(result => res.send(result))
})


//profile upload

router.put('/upload', auth.verify, (req,res) => {
    UserController.upload(req.body).then(result => res.send(result))
})

// [SECTION] Integration Routes

router.post('/verify-google-id-token', (req, res) => {
    UserController.verifyGoogleTokenId()
})

module.exports = router